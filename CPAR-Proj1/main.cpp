#include <iostream>
#include <omp.h>
#include <vector>
#include <ctime>
#include <chrono>
#include <papi.h>
#include "matrix.h"

using std::vector;

struct arguments
{
public:
	static unsigned int MATRIX_SIZE;
	/**
	 * functionID = 0 -> multLineByCols
	 * functionID = 1 -> multElemByLine
	 */
	static unsigned int FUNCTION_ID;
	static bool PARALLEL;
	static unsigned int THREADS;
};

unsigned int arguments::MATRIX_SIZE;
unsigned int arguments::FUNCTION_ID;
bool arguments::PARALLEL;
unsigned int arguments::THREADS;

void multLineByCols(matrix &m1, matrix &m2, matrix &m3)
{
	unsigned long dimension = m1.getHeight();
	for (unsigned long line = 0; line < dimension; line++)
	{
		for (unsigned long column = 0; column < dimension; column++)
		{
			for (unsigned long element = 0; element < dimension; element++)
			{
				m3(line, element) += m1(line, element) * m2(element, column);
			}
		}
	}
}

void multElemByLine(matrix &m1, matrix &m2, matrix &m3)
{
	for (unsigned long line = 0; line < m1.getHeight(); line++)
	{
		for (unsigned long column = 0; column < m1.getWidth(); column++)
		{
			for (unsigned long element = 0; element < m1.getWidth(); element++)
			{
				m3(line, column) += m1(line, column) * m2(line, element);
			}
		}
	}
}

void multLineByColsOMP(matrix &m1, matrix &m2, matrix &m3)
{
	auto const dimension = static_cast<int>(m1.getWidth());
#pragma omp parallel for
	for (int line = 0; line < dimension; line++)
	{
		for (int column = 0; column < dimension; column++)
		{
			for (int element = 0; element < dimension; element++)
			{
				auto lineUL = static_cast<unsigned long>(line);
				auto columnUL = static_cast<unsigned long>(column);
				auto elementUL = static_cast<unsigned long>(element);
				auto result = m1(lineUL, elementUL) * m2(elementUL, columnUL);
				m3(lineUL, elementUL) += result;
			}
		}
	}
}

void multElemByLineOMP(matrix &m1, matrix &m2, matrix &m3)
{
	auto height = static_cast<int>(m1.getHeight());
	auto width = static_cast<int>(m1.getWidth());
#pragma omp parallel for
	for (int line = 0; line < height; line++)
	{
		for (int column = 0; column < width; column++)
		{
			for (int element = 0; element < width; element++)
			{
				auto lineUL = static_cast<unsigned long>(line);
				auto columnUL = static_cast<unsigned long>(column);
				auto elementUL = static_cast<unsigned long>(element);
				m3(lineUL, columnUL) += m1(lineUL, columnUL) * m2(lineUL, elementUL);
			}
		}
	}
}

void printUsage()
{
    std::cout << "PROGRAM_NAME <matrix size> <function id> <parallel> <threads>\n";
}

void initArguments(int argc, char **argv)
{
	if(argc < 4)
	{
        printUsage();
		throw std::invalid_argument("Too few arguments.\n"); // NOLINT
	}

	if (argc > 5)
	{
        printUsage();
		throw std::invalid_argument("Too many arguments.\n"); // NOLINT
	}

	arguments::MATRIX_SIZE = static_cast<unsigned int>(std::stoul(argv[1]));
	arguments::FUNCTION_ID = static_cast<unsigned int>(std::stoul(argv[2]));
	arguments::PARALLEL = std::string("true") == std::string(argv[3]);

	if (!arguments::PARALLEL && argc > 4)
	{
		std::cout << "Threads value is unused because PARALLEL is not true.\n";
	}

    if(arguments::PARALLEL)
    {
        arguments::THREADS = static_cast<unsigned int>(std::stoul(argv[4]));
        omp_set_dynamic(0);
        omp_set_num_threads(arguments::THREADS);
    }
}

void getFunction(void (**function)(matrix &m1, matrix &m2, matrix &m3))
{
    switch (arguments::FUNCTION_ID)
    {
        case 1:
            arguments::PARALLEL ? *function = multLineByColsOMP : *function = multLineByCols;
            break;
        case 2:
            arguments::PARALLEL ? *function = multElemByLineOMP : *function = multElemByLine;
            break;
        default:
            throw std::invalid_argument("Bad function ID provided.\n"); // NOLINT
    }
}

int main(int argc, char **argv)
{
	initArguments(argc, argv);
	// Matrix initialization
	matrix m1 = matrix(arguments::MATRIX_SIZE);
	matrix m2 = matrix(arguments::MATRIX_SIZE);
	matrix m3 = matrix(arguments::MATRIX_SIZE);

	void (*function)(matrix &m1, matrix &m2, matrix &m3);
    getFunction(&function);

    int numberOfCounters = PAPI_num_counters();
    int Events[] = {PAPI_L1_TCM, PAPI_L2_TCM, PAPI_L3_TCM, PAPI_L2_TCW, PAPI_L3_TCW};
    int eventsAmount = sizeof(Events)/sizeof(Events[0]);

    if(eventsAmount > numberOfCounters)
    {
        throw std::exception();
    }

    long_long values[eventsAmount];

    std::chrono::steady_clock::time_point c_start = std::chrono::steady_clock::now();

    if (PAPI_start_counters(Events, eventsAmount) != PAPI_OK) {
        std::cout << "Error start.\n";
    }

    function(m1, m2, m3);

    if (PAPI_stop_counters(values, eventsAmount) != PAPI_OK) {
        std::cout << "Error stop.\n";
    }

    std::chrono::steady_clock::time_point c_end = std::chrono::steady_clock::now();
    std::chrono::duration<double> time_elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(
            c_end - c_start);
    std::string csv;
    std::cout << "Real Time: " << time_elapsed.count() << '\n';
    csv += std::to_string(time_elapsed.count()) + ';';
    for (int i = 0; i < eventsAmount; i++) {
        int Event = Events[i];
        long_long value = values[i];
        char eventString[PAPI_MAX_STR_LEN];
        if (PAPI_event_code_to_name(Event, eventString) != PAPI_OK) {
            std::cout << "Error Code To Name.\n";
        }
        std::cout << eventString << ": " << value << '\n';
        csv += std::to_string(value) + ";";
    }

	return 0;
}