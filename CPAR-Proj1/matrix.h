//
// Created by Francisco on 20/02/2018.
//

#ifndef CPAR_PROJ1_MATRIX_H
#define CPAR_PROJ1_MATRIX_H


#include <vector>

class matrix : std::vector<double>
{
private:
    unsigned long _width;
    unsigned long _height;

public:
    matrix(unsigned long width, unsigned long height) : std::vector<double>(width * height, 0)
    {
        _width = width;
        _height = height;
    }
    explicit matrix(unsigned long dimension) : std::vector<double>(dimension*dimension, 0)
    {
        _width = _height = dimension;
    }
    double& getMatrixElement(unsigned long i, unsigned long j)
    {
        return this->vector<double>::operator[](i * _width + j);
    }
    double setMatrixElement(unsigned long i, unsigned long j, double value)
    {
        getMatrixElement(i, j) = value;
    }
    bool isSquare()
    {
        return _width == _height;
    }
    unsigned long getWidth()
    {
        return _width;
    }
    unsigned long getHeight()
    {
        return _height;
    }
    double& operator()(unsigned long i, unsigned long j)
    {
        return getMatrixElement(i, j);
    }
    bool equalDimensions(matrix other)
    {
        return _width == other._width && _height == other._height;
    }
};


#endif //CPAR_PROJ1_MATRIX_H
