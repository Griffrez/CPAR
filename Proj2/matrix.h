//
// Created by Francisco on 10/04/2018.
//

#ifndef PROJ2_MATRIX_H
#define PROJ2_MATRIX_H


#include <vector>
#include <mutex>
#include <fstream>
#include <sstream>
#include <cmath>
#include <iostream>

class matrix : std::vector<double>
{
private:
    unsigned long _width;
    unsigned long _height;
    matrix() : std::vector<double>()
    {
        _width = 0;
        _height = 0;
    }

public:
    matrix(unsigned long width, unsigned long height) : std::vector<double>(width * height, 0)
    {
        _width = width;
        _height = height;
    }
    explicit matrix(unsigned long dimension) : std::vector<double>(dimension*dimension, 0)
    {
        _width = _height = dimension;
    }

    static matrix fromFileInfoSquare(const std::string &filename)
    {
        matrix retMat;
        std::ifstream fileStream(filename);
        std::string                line;
        while(std::getline(fileStream,line))
        {
            std::stringstream lineStream(line);
            std::string cell;
            while (std::getline(lineStream, cell, ','))
            {
                retMat.push_back(std::stod(cell));
            }
        }
        double squareRoot = sqrt(retMat.size());
        auto trancate = static_cast<int>(squareRoot);
        if(trancate == squareRoot)
        {
            retMat._height = static_cast<unsigned long>(squareRoot);
            retMat._width = static_cast<unsigned long>(squareRoot);
        }
        fileStream.close();
        return retMat;
    }
    double& getMatrixElement(unsigned long i, unsigned long j)
    {
        return this->vector<double>::operator[](i * _width + j);
    }
    void setMatrixElement(unsigned long i, unsigned long j, int value)
    {
        getMatrixElement(i, j) = value;
    }
    bool isSquare()
    {
        return _width == _height;
    }
    unsigned long getWidth()
    {
        return _width;
    }
    unsigned long getHeight()
    {
        return _height;
    }
    double& operator()(unsigned long i, unsigned long j)
    {
        return getMatrixElement(i, j);
    }
    bool equalDimensions(matrix other)
    {
        return _width == other._width && _height == other._height;
    }
    std::vector<matrix> lowerUpperDecomposition()
    {
        std::vector<matrix> ret = std::vector<matrix>();

        unsigned long dimension = getWidth();

        matrix lower = matrix(dimension);
        matrix upper = matrix(dimension);
        matrix& self = *this;

        for(int i = 0; i < dimension; i++)
        {
            lower(i, i) = 1;
        }

        for(int k = 0; k < dimension; k++)
        {
            //Upper
            for(int m = k; m < dimension; m++)
            {
                double sum = 0;
                for(int j = 0; j < k; j++)
                {
                    sum += lower(k, j) * upper(j, m);
                }
                upper(k, m) = self(k, m) - sum;
            }

            //Lower
            for(int i = k + 1; i < dimension; i++)
            {
                    double sum = 0;
                    for(int j = 0; j < k; j++)
                    {
                        sum += lower(i, j) * upper(j, k);
                    }
                    lower(i, k) = (self(i, k) - sum) / upper(k, k);
            }
        }

        ret.push_back(lower);
        ret.push_back(upper);

        return ret;
    }
    std::vector<matrix> lowerUpperDecompositionOMP()
    {
        std::vector<matrix> ret = std::vector<matrix>();

        unsigned long dimension = getWidth();

        matrix lower = matrix(dimension);
        matrix upper = matrix(dimension);
        matrix& self = *this;

        for(int i = 0; i < dimension; i++)
        {
            lower(i, i) = 1;
        }

        for(int k = 0; k < dimension; k++)
        {
            //Upper
#pragma omp parallel for
            for(int m = k; m < dimension; m++)
            {
                double sum = 0;
                for(int j = 0; j < k; j++)
                {
                    sum += lower(k, j) * upper(j, m);
                }
                upper(k, m) = self(k, m) - sum;
            }

            //Lower
#pragma omp parallel for
            for(int i = k + 1; i < dimension; i++)
            {
                double sum = 0;
                for(int j = 0; j < k; j++)
                {
                    sum += lower(i, j) * upper(j, k);
                }
                lower(i, k) = (self(i, k) - sum) / upper(k, k);
            }
        }

        ret.push_back(lower);
        ret.push_back(upper);

        return ret;
    }
    std::string toString()
    {
        std::string ret;
        for(unsigned long i = 0; i < _height; i++)
        {
            for(unsigned long j = 0; j < _width; j++)
            {
                ret += std::to_string((*this)(i, j)) + " ";
            }
        }
        return ret;
    }
    std::string toStringCSV()
    {
        std::string ret;
        for(unsigned long i = 0; i < _height; i++)
        {
            for(unsigned long j = 0; j < _width; j++)
            {
                double value = (*this)(i, j);
                std::string valueString = std::to_string(value);
                ret += valueString;
                if(j != _width - 1)
                {
                    ret += ',';
                }
            }
            if(i != _height - 1)
            {
                ret += '\n';
            }
        }
        return ret;
    }
    void set(double val)
    {
        for(double &value : *this)
        {
            value = val;
        }
    }
};


#endif //PROJ2_MATRIX_H
