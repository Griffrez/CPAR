#include <chrono>
#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>

std::vector<long long unsigned> eratosthenesSieve(long long unsigned limit)
{
    std::vector<bool> isPrime = std::vector<bool>(limit, true);
    isPrime[0] = false;
    auto const innerLimit = (long long unsigned)ceil(sqrt(limit));

    for(long long unsigned i = 0; i < innerLimit; i++)
    {
        if(isPrime[i])
        {
            for(long long unsigned j = 2 * i + 1; j < limit; j += i + 1)
            {
                isPrime[j] = false;
            }
        }
    }

    std::vector<long long unsigned> vec;
    for(long long unsigned i = 0; i < limit; i++)
    {
        if(isPrime[i])
        {
            vec.push_back(i + 1);
        }
    }

    return vec;
}

int main(int argc, char** argv) {
    unsigned long long limit = std::stoull(argv[1]);
    double sum_clock = 0;
    double sum_real = 0;
    int const runs = 1;
    std::vector<long long unsigned> vec;
    for(int i = 0; i < runs; i++)
    {
        std::chrono::steady_clock::time_point c_start = std::chrono::steady_clock::now();
        std::clock_t clock_start = clock();
        vec = eratosthenesSieve(limit);
        std::clock_t clock_end = clock();
        double seconds_clockO = (clock_end - clock_start) / (double) CLOCKS_PER_SEC;
        std::chrono::steady_clock::time_point c_end = std::chrono::steady_clock::now();
        std::chrono::duration<double> time_elapsedO = std::chrono::duration_cast<std::chrono::duration<double>>(
                c_end - c_start);
        sum_clock += seconds_clockO;
        sum_real += time_elapsedO.count();
    }
    std::cout << "CPU Time: " << sum_clock/runs << '\n';
    std::cout << "Real Time: " << sum_real/runs << '\n';

    std::ofstream primeFile("primes.csv", std::ios_base::out | std::ios_base::trunc);

    primeFile << vec[0];
    for(size_t i = 1; i < vec.size(); i++)
    {
        primeFile << "," << vec[i];
    }

    primeFile.close();

    return 0;
}

