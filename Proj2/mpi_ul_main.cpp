#include "matrix.h"
#include "mpi.h"

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    int rank; // MPI Rank
    int size; // World Size

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    matrix A = matrix::fromFileInfoSquare(argv[1]);
    /*matrix A(3);
    A(0,0) = 1;
    A(0,1) = 2;
    A(0,2) = 5;
    A(1,0) = 3;
    A(1,1) = 4;
    A(1,2) = 6;
    A(2,0) = 7;
    A(2,1) = 8;
    A(2,2) = 9;*/
    std::chrono::steady_clock::time_point c_start = std::chrono::steady_clock::now();
    std::clock_t clock_start = clock();
    matrix L(A.getWidth());
    for(int i = 0; i < A.getWidth(); i++)
    {
        L(i,i) = 1;
    }
    matrix U(A.getWidth());

    if(rank == 0)
    {
        unsigned long dimension = A.getWidth();
        MPI_Status mpi_status;
        std::vector<double> newValues;
        for(int k = 0; k < dimension; k++)
        {
            for(int process = 1; process < size; process++)
            {
                MPI_Probe(MPI::ANY_SOURCE, 0, MPI::COMM_WORLD, &mpi_status);
                int amount;
                MPI_Get_count(&mpi_status, MPI::DOUBLE, &amount);
                auto buffer = new double[amount];
                MPI_Recv(buffer, amount, MPI::DOUBLE, mpi_status.MPI_SOURCE, 0, MPI::COMM_WORLD, &mpi_status);
                for(int i = 0; i < amount; i++)
                {
                    newValues.push_back(buffer[i]);
                }
                delete buffer;
            }

            auto entries = static_cast<int>(newValues.size() / 4);
            double* buffer = &newValues[0];
            for(int i = 0; i < entries; i++)
            {
                double type = buffer[i*4];
                if(type == 0)
                {
                    U(buffer[i*4 + 1], buffer[i*4 + 2]) = buffer[i*4 + 3];
                }
                else if(type == 1)
                {
                    L(buffer[i*4 + 1], buffer[i*4 + 2]) = buffer[i*4 + 3];
                }
            }

            for(int process = 1; process < size; process++)
            {
                MPI_Send(&newValues[0], static_cast<int>(newValues.size()), MPI::DOUBLE, process, 1, MPI::COMM_WORLD);
            }
            newValues.clear();

            for(int process = 1; process < size; process++)
            {
                MPI_Probe(MPI::ANY_SOURCE, 0, MPI::COMM_WORLD, &mpi_status);
                int amount;
                MPI_Get_count(&mpi_status, MPI::DOUBLE, &amount);
                auto buffer = new double[amount];
                MPI_Recv(buffer, amount, MPI::DOUBLE, mpi_status.MPI_SOURCE, 0, MPI::COMM_WORLD, &mpi_status);
                for(int i = 0; i < amount; i++)
                {
                    newValues.push_back(buffer[i]);
                }
                delete buffer;
            }

            entries = static_cast<int>(newValues.size() / 4);
            buffer = &newValues[0];
            for(int i = 0; i < entries; i++)
            {
                double type = buffer[i*4];
                if(type == 0)
                {
                    U(buffer[i*4 + 1], buffer[i*4 + 2]) = buffer[i*4 + 3];
                }
                else if(type == 1)
                {
                    L(buffer[i*4 + 1], buffer[i*4 + 2]) = buffer[i*4 + 3];
                }
            }

            for(int process = 1; process < size; process++)
            {
                MPI_Send(&newValues[0], static_cast<int>(newValues.size()), MPI::DOUBLE, process, 1, MPI::COMM_WORLD);
            }
            newValues.clear();
        }

        std::clock_t clock_end = clock();
        double seconds_clockO = (clock_end - clock_start)/(double)CLOCKS_PER_SEC;
        std::chrono::steady_clock::time_point c_end = std::chrono::steady_clock::now();
        std::chrono::duration<double> time_elapsedO = std::chrono::duration_cast<std::chrono::duration<double>>(
                c_end - c_start);

        std::cout << "CPU Time: " << seconds_clockO << '\n';
        std::cout << "Real Time: " << time_elapsedO.count() << '\n';

        std::ofstream lowerFile("L.csv", std::ios_base::out | std::ios_base::trunc);
        std::ofstream upperFile("U.csv", std::ios_base::out | std::ios_base::trunc);

        lowerFile << L.toStringCSV();
        upperFile << U.toStringCSV();
    }
    else
    {
        unsigned long dimension = A.getWidth();
        double cellsPerProcess = (double)dimension / (size - 1);
        double count = 0;
        for(int i = 1; i < rank; i++)
        {
            count += cellsPerProcess;
        }
        auto cellStart = (int)count;
        auto cellEnd = (int)(count + cellsPerProcess);
        int min;
        int max;
        bool first = true;
        for(int i = cellStart; i < cellEnd; i++)
        {
            if(i <= cellEnd)
            {
                if(first)
                {
                    first = false;
                    min = i;
                }
                max = i;
            }
        }

        std::vector<double> data;

        int k = 0;
        while(k < dimension)
        {
            MPI_Status mpi_status;
            for(int i = min; i <= max; i++)
            {
                if (i >= k) {
                    double sum = 0;
                    for (int j = 0; j < k; j++) {
                        sum += L(k, j) * U(j, i);
                    }
                    double value = A(k, i) - sum;
                    data.push_back(0);
                    data.push_back(k);
                    data.push_back(i);
                    data.push_back(value);
                }
            }

            MPI_Send(&data[0], static_cast<int>(data.size()), MPI::DOUBLE, 0, 0, MPI::COMM_WORLD);
            MPI_Probe(0, 1, MPI::COMM_WORLD, &mpi_status);
            int amount;
            MPI_Get_count(&mpi_status, MPI::DOUBLE, &amount);
            auto buffer = new double[amount];
            MPI_Recv(buffer, amount, MPI::DOUBLE, 0, 1, MPI::COMM_WORLD, &mpi_status);
            int entries = amount / 4;
            for(int i = 0; i < entries; i++)
            {
                double type = buffer[i*4];
                if(type == 0)
                {
                    U(buffer[i*4 + 1], buffer[i*4 + 2]) = buffer[i*4 + 3];
                }
                else if(type == 1)
                {
                    L(buffer[i*4 + 1], buffer[i*4 + 2]) = buffer[i*4 + 3];
                }
            }
            delete buffer;
            data.clear();

            for(int i = min; i <= max; i++)
            {
                if(i > k)
                {
                    double sum = 0;
                    for (int j = 0; j < k; j++)
                    {
                        sum += L(i, j) * U(j, k);
                    }
                    double value = (A(i, k) - sum) / U(k, k);
                    data.push_back(1);
                    data.push_back(i);
                    data.push_back(k);
                    data.push_back(value);
                }
            }

            MPI_Send(&data[0], static_cast<int>(data.size()), MPI::DOUBLE, 0, 0, MPI::COMM_WORLD);
            MPI_Probe(0, 1, MPI::COMM_WORLD, &mpi_status);
            MPI_Get_count(&mpi_status, MPI::DOUBLE, &amount);
            buffer = new double[amount];
            MPI_Recv(buffer, amount, MPI::DOUBLE, 0, 1, MPI::COMM_WORLD, &mpi_status);
            entries = amount / 4;
            for(int i = 0; i < entries; i++)
            {
                double type = buffer[i*4];
                if(type == 0)
                {
                    U(buffer[i*4 + 1], buffer[i*4 + 2]) = buffer[i*4 + 3];
                }
                else if(type == 1)
                {
                    L(buffer[i*4 + 1], buffer[i*4 + 2]) = buffer[i*4 + 3];
                }
            }
            delete buffer;
            data.clear();
            k++;
        }
    }

    MPI_Finalize();
    return 0;
}

