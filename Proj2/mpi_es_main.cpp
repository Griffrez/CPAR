#include "matrix.h"
#include "mpi.h"

int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    int rank; // MPI Rank
    int size; // World Size

    MPI_Comm_rank(MPI::COMM_WORLD, &rank);
    MPI_Comm_size(MPI::COMM_WORLD, &size);

    unsigned long limit = std::stoul(argv[1]);

    if(rank == 0)
    {
        std::chrono::steady_clock::time_point c_start = std::chrono::steady_clock::now();
        std::clock_t clock_start = clock();
        std::vector<bool> isPrime(limit, true);
        isPrime[0] = false;
        unsigned long iter = 0;
        auto innerLimit = static_cast<unsigned long>(ceil(sqrt(limit)));
        int terminated = 0;
        for(unsigned i = 1; i < size; i++)
        {
            while(iter < innerLimit && isPrime[iter] == false)
            {
                iter++;
            }

            if(iter >= innerLimit)
            {
                char c = 1;
                MPI_Send(&c, 1, MPI::CHAR, i, 1, MPI::COMM_WORLD);
                terminated++;
                continue;
            }

            MPI_Send(&iter, 1, MPI::UNSIGNED_LONG, i, 0, MPI::COMM_WORLD);
            iter++;
        }

        while(true)
        {
            MPI_Status mpi_status;
            MPI_Probe(MPI::ANY_SOURCE, 0, MPI::COMM_WORLD, &mpi_status);
            int amount;
            MPI_Get_count(&mpi_status, MPI::UNSIGNED_LONG, &amount);
            auto buffer = new unsigned long[amount];
            MPI_Recv(buffer, amount, MPI::UNSIGNED_LONG, mpi_status.MPI_SOURCE, mpi_status.MPI_TAG, MPI::COMM_WORLD, &mpi_status);
            for(unsigned long i = 0; i < amount; i++)
            {
                isPrime[buffer[i]] = false;
            }
            delete buffer;

            while (iter < innerLimit && isPrime[iter] == false)
            {
                iter++;
            }

            if(iter >= innerLimit)
            {
                char c = 1;
                MPI_Send(&c, 1, MPI::CHAR, mpi_status.MPI_SOURCE, 1, MPI::COMM_WORLD);
                terminated++;

                if(terminated >= size - 1)
                {
                    std::clock_t clock_end = clock();
                    double seconds_clockO = (clock_end - clock_start)/(double)CLOCKS_PER_SEC;
                    std::chrono::steady_clock::time_point c_end = std::chrono::steady_clock::now();
                    std::chrono::duration<double> time_elapsedO = std::chrono::duration_cast<std::chrono::duration<double>>(
                            c_end - c_start);

                    std::cout << "CPU Time: " << seconds_clockO << '\n';
                    std::cout << "Real Time: " << time_elapsedO.count() << '\n';

                    std::vector<size_t> vec;
                    for(size_t i = 0; i < isPrime.size(); i++)
                    {
                        if(isPrime[i])
                        {
                            vec.push_back(i + 1);
                        }
                    }

                    std::ofstream primeFile("primes.csv", std::ios_base::out | std::ios_base::trunc);

                    primeFile << vec[0];
                    for(size_t i = 1; i < vec.size(); i++)
                    {
                        primeFile << "," << vec[i];
                    }

                    break;
                }
            }

            MPI_Send(&iter, 1, MPI::UNSIGNED_LONG, mpi_status.MPI_SOURCE, 0, MPI::COMM_WORLD);
            iter++;
        }
    }
    else
    {
        MPI_Status mpi_status;
        while(true)
        {
            MPI_Probe(0, MPI::ANY_TAG, MPI::COMM_WORLD, &mpi_status);
            if(mpi_status.MPI_TAG == 0)
            {
                unsigned long prime;
                MPI_Recv(&prime, 1, MPI::UNSIGNED_LONG, 0, 0, MPI::COMM_WORLD, &mpi_status);

                std::vector<unsigned long> nonPrimes;
                for (unsigned long start = prime * 2 + 1; start < limit; start += prime + 1)
                {
                    nonPrimes.push_back(start);
                }

                MPI_Send(&nonPrimes[0], static_cast<int>(nonPrimes.size()), MPI::UNSIGNED_LONG, 0, 0, MPI::COMM_WORLD);
            }
            else
            {
                char c;
                MPI_Recv(&c, 1, MPI::CHAR, 0, MPI::ANY_TAG, MPI::COMM_WORLD, &mpi_status);
                break;
            }
        }
    }

    MPI_Finalize();
    return 0;
}
