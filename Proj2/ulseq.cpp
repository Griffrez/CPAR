#include <iostream>
#include "matrix.h"

int main(int argc, char** argv) {
    matrix myMat = matrix::fromFileInfoSquare(argv[1]);

    double sum_clock = 0;
    double sum_real = 0;
    int const runs = 1;
    std::vector<matrix> vec;
    for(int i = 0; i < runs; i++)
    {
        std::chrono::steady_clock::time_point c_start = std::chrono::steady_clock::now();
        std::clock_t clock_start = clock();
        vec = myMat.lowerUpperDecomposition();
        std::clock_t clock_end = clock();
        double seconds_clockO = (clock_end - clock_start) / (double) CLOCKS_PER_SEC;
        std::chrono::steady_clock::time_point c_end = std::chrono::steady_clock::now();
        std::chrono::duration<double> time_elapsedO = std::chrono::duration_cast<std::chrono::duration<double>>(
                c_end - c_start);
        sum_clock += seconds_clockO;
        sum_real += time_elapsedO.count();
    }
    std::cout << "CPU Time: " << sum_clock/runs << '\n';
    std::cout << "Real Time: " << sum_real/runs << '\n';

    matrix lower = vec[0];
    matrix upper = vec[1];

    std::ofstream lowerFile("L.csv", std::ios_base::out | std::ios_base::trunc);
    std::ofstream upperFile("U.csv", std::ios_base::out | std::ios_base::trunc);

    lowerFile << lower.toStringCSV();
    upperFile << upper.toStringCSV();

    lowerFile.close();
    upperFile.close();

    return 0;
}

