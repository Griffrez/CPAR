#include <mpi.h>
#include <stdio.h>

int main(int argc, char** argv)
{
	MPI_Init(&argc, &argv);
	int size;
	int rank;
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	printf("Hello World from %d of %d.", size, rank);
	MPI_Finalize();
	return 0;
}